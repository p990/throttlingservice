package foo;

public interface Clock {

    long currentTimeMillis();
}
