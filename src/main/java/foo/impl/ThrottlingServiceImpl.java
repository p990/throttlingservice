package foo.impl;


import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import foo.Clock;
import foo.SLA;
import foo.SlaService;
import foo.ThrottlingService;

public class ThrottlingServiceImpl implements ThrottlingService {

  private final ConcurrentHashMap<String, SLA> userByToken = new ConcurrentHashMap<>();
  private final ConcurrentHashMap<String, LastRequestsTimeCache> reqsByUser = new ConcurrentHashMap<>();

  private final SlaService slaService;
  private Clock clock = System::currentTimeMillis;

  public ThrottlingServiceImpl(SlaService slaService) {
    this.slaService = slaService;
  }

  @Override
  public boolean isRequestAllowed(Optional<String> inputToken) {
    SLA curSla = userByToken.computeIfAbsent(inputToken.orElse(slaService.getGuestSla().getUser()), t -> {
      slaService.getSlaByToken(t).thenAccept(sla -> userByToken.put(t, sla));
      return slaService.getGuestSla();
    });

    LastRequestsTimeCache lastRequestsTimeCache = reqsByUser.computeIfAbsent(curSla.getUser(), k -> new LastRequestsTimeCache(curSla, clock));
    return lastRequestsTimeCache.isRequestAllowed();
  }

  public void setClock(Clock clock) {
    this.clock = clock;
  }
}
