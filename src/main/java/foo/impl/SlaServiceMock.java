package foo.impl;

import foo.SLA;
import foo.SlaService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class SlaServiceMock implements SlaService {
    private boolean testMode = false;
    private static final Map<String, SLA> data = new HashMap<>();
    private Set<String> readyTokens = new HashSet<>();
    private final SLA guestSla;

    static {
        data.put("token1user1", new SLA("user1", 30));
        data.put("token1vasa", new SLA("vasa", 5));
        data.put("token1user2", new SLA("user2", 15));
        data.put("token2user2", new SLA("user2", 15));
    }

    SlaServiceMock(int guestRpc){
        guestSla = new SLA("guest", guestRpc);
    }

    public SLA getGuestSla() {
        return guestSla;
    }

    @Override
    public CompletableFuture<SLA> getSlaByToken(String token) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                if (testMode) {
                    while (!readyTokens.contains(token)) {
                        TimeUnit.MILLISECONDS.sleep(1);
                    }
                } else {
                    int randomPause = ThreadLocalRandom.current().nextInt(100, 300 + 1);
                    TimeUnit.MILLISECONDS.sleep(randomPause);
                }
                return data.getOrDefault(token, guestSla);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        });
    }

    public void applyTestMode() {
        testMode = true;
    }

    public void invokeExtractSLAForToken(String token) {
        readyTokens.add(token);
        try {
            TimeUnit.MILLISECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
