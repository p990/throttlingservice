package foo.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ThrottlingServiceImplTest {

    private ThrottlingServiceImpl throttlingService;
    private ClockMock clockMock = new ClockMock();
    private SlaServiceMock slaService ;

    @BeforeEach
    void before() {
        slaService = new SlaServiceMock(10);
        slaService.applyTestMode();

        throttlingService = new ThrottlingServiceImpl( slaService);
        throttlingService.setClock(clockMock);
    }

    @Test
    void guestRequestsIsRequestAllowedTest() {
        for (int i = 10; i < 20; i++) {
            clockMock.setCurrentTime(i);
            assertTrue(throttlingService.isRequestAllowed(Optional.empty()));
        }
        assertFalse(throttlingService.isRequestAllowed(Optional.empty()));
        clockMock.setCurrentTime(1010);
        assertFalse(throttlingService.isRequestAllowed(Optional.empty()));
        clockMock.setCurrentTime(1011);
        assertTrue(throttlingService.isRequestAllowed(Optional.empty()));
    }

    @Test
    void incorrectTokenRequestsIsRequestAllowedTest() {
        Optional<String> incorrectToken = Optional.of("incorrect Token");
        for (int i = 10; i < 20; i++) {
            clockMock.setCurrentTime(i);
            assertTrue(throttlingService.isRequestAllowed(incorrectToken));
        }
        assertFalse(throttlingService.isRequestAllowed(incorrectToken));
        clockMock.setCurrentTime(1010);
        assertFalse(throttlingService.isRequestAllowed(incorrectToken));
        clockMock.setCurrentTime(1011);
        assertTrue(throttlingService.isRequestAllowed(incorrectToken));
    }



    @Test
    void authorizedUserButSlaServiceNotReadyRequestsIsRequestAllowedTest() {
        Optional<String> token1user1 = Optional.of("token1user1");
        for (int i = 10; i < 20; i++) {
            clockMock.setCurrentTime(i);
            assertTrue(throttlingService.isRequestAllowed(token1user1));
        }
        assertFalse(throttlingService.isRequestAllowed(token1user1));
    }

    @Test
    void authorizedUserRequestsIsRequestAllowedTest() throws InterruptedException {
        Optional<String> token1user1 = Optional.of("token1user1");
        for (int i = 10; i < 48; i++) {
            clockMock.setCurrentTime(i);
            if(i==18){
                slaService.invokeExtractSLAForToken(token1user1.get());
            }
            assertTrue(throttlingService.isRequestAllowed(token1user1));
        }
        assertFalse(throttlingService.isRequestAllowed(token1user1));
        clockMock.setCurrentTime(1010);
        assertFalse(throttlingService.isRequestAllowed(token1user1));
        clockMock.setCurrentTime(1019);
        assertTrue(throttlingService.isRequestAllowed(token1user1));
    }

    @Test
    void authorizedUserWithTwoTokensRequestsIsRequestAllowedTest() throws InterruptedException {
        Optional<String> token1user2 = Optional.of("token1user2");
        Optional<String> token2user2 = Optional.of("token2user2");
        for (int i = 10; i < 15; i++) {
            clockMock.setCurrentTime(i);
            assertTrue(throttlingService.isRequestAllowed(token1user2));
            assertTrue(throttlingService.isRequestAllowed(token2user2));
        }
        slaService.invokeExtractSLAForToken(token1user2.get());
        slaService.invokeExtractSLAForToken(token2user2.get());
        for (int i = 10; i < 17; i++) {
            //first token retrived SLA
            assertTrue(throttlingService.isRequestAllowed(token1user2));
            //second token retrived SLA and merge count of requests
            assertTrue(throttlingService.isRequestAllowed(token2user2));
        }
        assertTrue(throttlingService.isRequestAllowed(token1user2));
        //rich limit
        assertFalse(throttlingService.isRequestAllowed(token2user2));

        clockMock.setCurrentTime(1011);
        assertFalse(throttlingService.isRequestAllowed(token1user2));
        clockMock.setCurrentTime(1012);
        assertFalse(throttlingService.isRequestAllowed(token1user2));
        clockMock.setCurrentTime(1013);
        assertFalse(throttlingService.isRequestAllowed(token1user2));
        clockMock.setCurrentTime(1015);
        assertTrue(throttlingService.isRequestAllowed(token1user2));
    }
}

